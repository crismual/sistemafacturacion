/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.usbcali.facturacion.vista;

import co.edu.usbcali.facturacion.delegado.DelegadoNegocio;
import co.edu.usbcali.facturacion.output.FactuacionOutput;
import java.util.Date;

/**
 *
 * @author crism
 */
public class SistemaFacturacion {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        DelegadoNegocio delegadoNegocio = new DelegadoNegocio();
        FactuacionOutput facturacion = new FactuacionOutput();
        
        facturacion = delegadoNegocio.obtenerFacturacionCliente(new Date(), new Date(), 0);
        
        System.out.println(facturacion.getTotalEjecutivo());

    }

}
