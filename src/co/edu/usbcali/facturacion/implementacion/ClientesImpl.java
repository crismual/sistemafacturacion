/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.usbcali.facturacion.implementacion;

import co.edu.usbcali.facturacion.modelo.Cliente;
import co.edu.usbcali.facturacion.output.FactuacionOutput;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author crism
 */
public class ClientesImpl {

    List<Cliente> clientes = new ArrayList<>();

    public Integer crearCliente(Cliente cliente) {
        
        /* 
            - se valida cada uno de los atributos que no deberian de ser nulos ni vacios
            - n caso de que alguno sea vacio o nulo retorna cero y en caso de guardar exitoso retorna 1
            - se deven de validar todos los atributos para poder tener datos consistentes
        */

        if (cliente.getCedula() == null) {
            return 0;
        }
        if (cliente.getNombre() == null  || cliente.getNombre().equals("")) {
            return 0;
        }
        if (cliente.getTipoPago() == null  || cliente.getTipoPago().equals("")) {
            return 0;
        }
        if (cliente.getTipoPersona() == null  || cliente.getTipoPersona().equals("")) {
            return 0;
        }
        
        clientes.add(cliente);

        return 1;
    }
    
    public List obtenerClientes(){
        return clientes;
    }
    
    public FactuacionOutput obtenerFacturacionCliente(Date fechaInicial, Date fechaFinal, long cedulaCliente){
        
        FactuacionOutput facturacion = new FactuacionOutput();
        
        facturacion.setTotalEjecutivo(new Long("30000"));
        
        return facturacion;
    }

}
