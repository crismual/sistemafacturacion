/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.usbcali.facturacion.output;

/**
 *
 * @author crism
 */
public class FactuacionOutput {

    private long totalHospedaje;
    private long totalRestaurante;
    private long totalSocial;
    private long totalTuristico;
    private long totalSpa;
    private long totalEjecutivo;
    private long totalRecreacion;
    private long totalModulos;
    
    

    /**
     * @return the totalHospedaje
     */
    public long getTotalHospedaje() {
        return totalHospedaje;
    }

    /**
     * @param totalHospedaje the totalHospedaje to set
     */
    public void setTotalHospedaje(long totalHospedaje) {
        this.totalHospedaje = totalHospedaje;
    }

    /**
     * @return the totalRestaurante
     */
    public long getTotalRestaurante() {
        return totalRestaurante;
    }

    /**
     * @param totalRestaurante the totalRestaurante to set
     */
    public void setTotalRestaurante(long totalRestaurante) {
        this.totalRestaurante = totalRestaurante;
    }

    /**
     * @return the totalSocial
     */
    public long getTotalSocial() {
        return totalSocial;
    }

    /**
     * @param totalSocial the totalSocial to set
     */
    public void setTotalSocial(long totalSocial) {
        this.totalSocial = totalSocial;
    }

    /**
     * @return the totalTuristico
     */
    public long getTotalTuristico() {
        return totalTuristico;
    }

    /**
     * @param totalTuristico the totalTuristico to set
     */
    public void setTotalTuristico(long totalTuristico) {
        this.totalTuristico = totalTuristico;
    }

    /**
     * @return the totalSpa
     */
    public long getTotalSpa() {
        return totalSpa;
    }

    /**
     * @param totalSpa the totalSpa to set
     */
    public void setTotalSpa(long totalSpa) {
        this.totalSpa = totalSpa;
    }

    /**
     * @return the totalEjecutivo
     */
    public long getTotalEjecutivo() {
        return totalEjecutivo;
    }

    /**
     * @param totalEjecutivo the totalEjecutivo to set
     */
    public void setTotalEjecutivo(long totalEjecutivo) {
        this.totalEjecutivo = totalEjecutivo;
    }

    /**
     * @return the totalRecreacion
     */
    public long getTotalRecreacion() {
        return totalRecreacion;
    }

    /**
     * @param totalRecreacion the totalRecreacion to set
     */
    public void setTotalRecreacion(long totalRecreacion) {
        this.totalRecreacion = totalRecreacion;
    }

    /**
     * @return the totalModulos
     */
    public long getTotalModulos() {
        return totalModulos;
    }

    /**
     * @param totalModulos the totalModulos to set
     */
    public void setTotalModulos(long totalModulos) {
        this.totalModulos = totalModulos;
    }

}
