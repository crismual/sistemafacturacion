/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.usbcali.facturacion.delegado;

import co.edu.usbcali.facturacion.implementacion.ClientesImpl;
import co.edu.usbcali.facturacion.implementacion.EjecutivoImpl;
import co.edu.usbcali.facturacion.implementacion.HospedajeImpl;
import co.edu.usbcali.facturacion.implementacion.RecreacionImpl;
import co.edu.usbcali.facturacion.implementacion.RestauranteImpl;
import co.edu.usbcali.facturacion.implementacion.SocialImpl;
import co.edu.usbcali.facturacion.implementacion.SpaImpl;
import co.edu.usbcali.facturacion.implementacion.TurismoImpl;
import co.edu.usbcali.facturacion.modelo.Cliente;
import co.edu.usbcali.facturacion.output.FactuacionOutput;
import java.util.Date;
import java.util.List;
/**
 *
 * @author crism
 */
public class DelegadoNegocio {
    
    public ClientesImpl clientes = new ClientesImpl();
    public EjecutivoImpl ejecutivo = new EjecutivoImpl();
    public HospedajeImpl hospedaje = new HospedajeImpl();
    public RecreacionImpl recreacion = new RecreacionImpl();
    public RestauranteImpl restaurante = new RestauranteImpl();
    public SocialImpl social = new SocialImpl();
    public SpaImpl spa = new SpaImpl();
    public TurismoImpl turismo = new TurismoImpl();

    public Integer crearCliente(Cliente cliente) {
        return clientes.crearCliente(cliente);
    }
        
    public List obtenerClientes(){
        return clientes.obtenerClientes();
    }
    
    public FactuacionOutput obtenerFacturacionCliente(Date fechaInicial, Date fechaFinal, long cedulaCliente){
        return clientes.obtenerFacturacionCliente(fechaInicial, fechaFinal, cedulaCliente);
    }
    
}
